#!/usr/bin/env python

import os
import uuid

from cookiecutter.main import cookiecutter

TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m[WARNING]: "
INFO = "\x1b[1;33m[INFO]: "
HINT = "\x1b[3;33m"
SUCCESS = "\x1b[1;32m[SUCCESS]: "

print(INFO + "Welcome to the SRCP Project builder.\n" + TERMINATOR)
print(INFO + "Default answers to questions are in []" + TERMINATOR)
print(INFO + "Answer the following questions:\n" + TERMINATOR)

cookiecutter(
    'test',
    extra_context={'account_id': str(uuid.uuid4().hex[:11]).lower(), 'platform_id': str(uuid.uuid4().hex[:11]).lower()}
)

print(SUCCESS + "Project initialized, keep up the good work!" + TERMINATOR)