import os

# Nice formatting

TERMINATOR = "\x1b[0m"
WARNING = "\x1b[1;33m[WARNING]: "
INFO = "\x1b[1;33m[INFO]: "
HINT = "\x1b[3;33m"
SUCCESS = "\x1b[1;32m[SUCCESS]: "


print(WARNING + "post_gen_script" + TERMINATOR)