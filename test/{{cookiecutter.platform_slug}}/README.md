# Description


This is a template to deploy and configure a SRCP Virtual HPC platform on [OpenStack](https://wiki.openstack.org/wiki/Heat).
This template uses [terraform](https://) templates and modules to deploy all infrastructure elements, and Ansible to configure them.
Ansible inventory is dynamically created using a custom inventory scripts.

# Options
Generated from a cookie cutter template this platform has the following options selected:

## Environment
* Environment: {{cookiecutter.environment}}
{% if cookiecutter.login_public_ip == "CUDN-Public" %}
* Login Node CUDN Public IP (128.x.x.x): yes
{% elif cookiecutter.login_public_ip == "CUDN-Private" %} 
* Login Node CUDN Private (172.x.x.x) IP: yes
{% endif %}
{% if cookiecutter.upload_details == "y" %}
* Details uploaded into database
{% elif cookiecutter.upload_details == "n" %} 
* Details not uploaded into database
{% endif %}
{% if cookiecutter.dm_module == "y" %}


## Data Management Gateway
* Data Mangement Gateway: Allows airlocked transfer of data"
{% endif %}

## Slurm Compute Settings
* Slurm Compute Node Instance flavor: {{cookiecutter.compute_image}}
* Slurm Compute Node Count: {{cookiecutter.compute_image}}s