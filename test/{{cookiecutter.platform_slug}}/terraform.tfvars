environment = "{{cookiecutter.environment}}"

account_id = "{{cookiecutter.account_id}}"
platform_id = "{{cookiecutter.platform_id}}"

project_label = "{{cookiecutter.platform_slug}} ({{cookiecutter.environment}})"

login_public_ip: "{{cookiecutter.login_public_ip}}"
data: "{{cookiecutter.dm_module}}"


#Do you want to install OOD?
install_ood = "yes"

#If the above is true you'll need a cname
{% if cookiecutter.login_public_ip == "CUDN-Public" %}
public_url = "{{cookiecutter.platform_slug}}.srcp.hpc.cam.ac.uk"
{%elif cookiecutter.login_public_ip == "CUDN-Private" %}
public_url = "{{cookiecutter.platform_slug}}.srcp.hpc.private.cam.ac.uk"
{% endif %}

# Images and Flavors
images: {
{% if cookiecutter.dm_module == "y" %}  "data_gateway"  = "CentOS-7-x86_64-GenericCloud"{% endif %}
  "bastion"      = "CentOS-7-x86_64-GenericCloud"
  "login"        = "CentOS-7-x86_64-GenericCloud"
  "nfs"          = "CentOS-7-x86_64-GenericCloud"
}

flavors = {
  "bastion"       = "C1.vss.tiny"
  "login"         = "C1.vss.tiny"
  "nfs"           = "C1.vss.tiny"
  "slurm_compute" = "{{cookiecutter.compute_image}}"
}
# Compute Node CountS
compute_nodes = {{cookiecutter.compute_nodes}}

# External service 
bareos_director_fqdn = "dev-bareos.hpc.private.cam.ac.uk"

bareos_director_service_name = "bareos-dir-dev"